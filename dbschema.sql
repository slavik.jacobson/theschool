CREATE DATABASE theschool;

USE theschool;

CREATE TABLE courses (
  id int(8) UNIQUE AUTO_INCREMENT,
  name varchar(48) UNIQUE NOT NULL,
  description varchar(255),
  image varchar(255),
  PRIMARY KEY (id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE students (
  id int(8) UNIQUE AUTO_INCREMENT,
  name varchar(64) NOT NULL,
  phone varchar(20) NOT NULL,
  email varchar(20) UNIQUE NOT NULL,
  image varchar (500),
  PRIMARY KEY (id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE students_to_courses (
  course_id int(8) NOT NULL,
  student_id int(8) NOT NULL,
  PRIMARY KEY (course_id,student_id),
  FOREIGN KEY (course_id) REFERENCES courses(id) ON DELETE CASCADE,
  FOREIGN KEY (student_id) REFERENCES students(id) ON DELETE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE admin (
  id int(16) UNIQUE AUTO_INCREMENT,
  name varchar(64) NOT NULL,
  phone varchar(20) NOT NULL,
  role enum('owner','manager','sales') NOT NULL,
  email varchar(20) UNIQUE NOT NULL,
  password varchar(64) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

INSERT INTO admin (name, phone, email, password, role)
VALUES ('admin', '037100741', 'omer.morad@rocks.com', '$2y$10$O/lP/bgk43gUAl8zsnHX/uLmeTPU8ZPhMpMKfNieArvF71QfLhIIu','owner')

