# PHP JBH TheSchool job backend

## About

Done with Slim 3.0 , comes with DB installed and PHP admin installed

PHP changes, Slim changes, and this was attempt to _"Nodify"_ PHP backend style-wise.

## Requirements and how to install

Requires docker with docker-compose.

1. `git clone https://slavik.jacobson@gitlab.com/slavik.jacobson/theschool.git && cd theschool && docker-compose up -d`

 It will:

1. Run PHP slim 3.0 server on port 9001 - `localhost:9001/`.
2. Provide MySQL database with schema and data inside.
3. Provide PHPMyAdmin interface available at port 8080 - `localhost:8080/`, user : **root**, pw: **helloomer**;


## Authentication

Api implements [HTTP Basic Authentication](https://en.wikipedia.org/wiki/Basic_access_authentication) protocol (which is one of more fitting simple protocols for servers that used only for API)

In order to authenticate client of the API must send `Authorization: Basic <base64encodedCredentials>` header where base64encodedCredentials are string of `<adminName>:<adminPassword>`.


For example for admin **hello** with password **world** header will be `Authorization: Basic aGVsbG86d29ybGQ=` - where `aGVsbG86d29ybGQ=` ius simple base64 encoded `hello:world` string.

**Failed Auth Response**

Status: **401**.

```json
{"status":"error","message":"Authentication Failed"}
```

## API
---



For each POST/PUT request validation rules are same. All required fields in POST also required in PUT.
Validation rules are simple and stated in example jsons.
Failed Validation ends with reponse with status **422**.

```json
{"status":"error","message":"Invalid data"}
```

Scoping follows rules stated in work description PDF. For more details please refer to it.
Failed Scoping results with reponse of status **403**

```json
{"status":"error","message":"Action Forbidden"}
```

Bad request status response: **400**

```json
{"status":"error","message":"Bad Request","details":"Duplicate entry 'hadasdfst@jbt.com' for key 'email'"}
```

---
---

### Admin

Api endpoint : `/admin`

---

#### _getAll_

Admin getAll method have passwords removed

**Request**

URL: `GET localhost:9001/admin`

**Response**

Status: **200**

```json
[{"id":"1","name":"admin","phone":"037100741","email":"omer.morad@rocks.com","role":"owner"},
{"id":"2","name":"Hadas","phone":"04543534543","email":"hadas.jbt@jbt.com","role":"manager"}]
```

---

#### _get_

**Request**

URL: `GET localhost:9001/admin/2`

**Response**

Status: **200**

```json
{"id":"2","name":"Hadas","phone":"04543534543","email":"hadas.jbt@jbt.com","password":"$2y$10$3Lp6GNdp3OAnRIpWOv\/hZ.MOxBzm2vuA2sdeDkt3Sfp8fSwaRnZ2K","role":"manager"}
```

---

#### _create_

**Request**

URL: `POST localhost:9001/admin`

```javascript
// expected keys ['name','phone','role','image','email','password']
{
"name":"45sdfsdfsd3s",  // required
"phone":"03345453",
"role":"manager", // must be correct enum ('manager','sales'), required
"email":"had3gfgt@jbt.com", //must be valid email, required
"password":"helloomer"
}
```

**Response**

Status: **201**

```json
{"status":"created","message":"Created","id":"15"}
```

---

#### _update_

**Request**

URL: `PUT localhost:9001/admin/15`

```javascript
// expected keys ['name','phone','role','image','email','password']
{
"name":"45sdfsdfsd3s",  // required
"phone":"03345453",
"role":"manager", // must be correct enum ('manager','sales'), required
"email":"had3gfgt@jbt.com", //must be valid email, required
"password":"helloomer"
}
```

**Response**

Status: **200**

```json
{"status":"ok","message":"Ok"}
```

---

#### _delete_

**Request**

URL: `DELETE localhost:9001/admin/2`

**Response**

Status: **204**

```json
{"status":"ok","message":"Ok"}
```

---
---

### Courses

Api endpoint : `/courses`

---

#### _getAll_

**Request**

URL: `GET localhost:9001/courses`

**Response**

Status: **200**

```json
[{"id":"22","name":"V4545454554la","description":"0454565634543","image":null},
{"id":"23","name":"V454544545554la","description":"04545wwww34543","image":null},
{"id":"24","name":"sasasas","description":"cacasdasdasdasdasdas","image":"adasdasdsad"}]
```

---

#### _get_

**Request**

URL: `GET localhost:9001/courses/24`

**Response**

Status: **200**

```json
{"id":"24","name":"sasasas","description":"cacasdasdasdasdasdas","image":"adasdasdsad"}
```

---

#### _create_

**Request**

URL: `POST localhost:9001/courses`

```javascript
// expected keys ['name','description','image']
{
"name":"45sdfsdfsd3s",  // required
"image":"http://google.com/image.png",
"description":"Heya Omer!"
}
```

**Response**

Status: **201**

```json
{"status":"created","message":"Created","id":"15"}
```

---

#### _update_

**Request**

URL: `PUT localhost:9001/courses/15`

```javascript
// expected keys ['name','description','image']
{
"name":"45sdfsdfsd3s",  // required
"image":"http://google.com/image.png",
"description":"Heya Omer!"
}
```

**Response**

Status: **200**

```json
{"status":"ok","message":"Ok"}
```

---

#### _delete_

**Request**

URL: `DELETE localhost:9001/courses/2`

**Response**

Status: **204**

```json
{"status":"ok","message":"Ok"}
```

---
---

### Students

Api endpoint : `/students`

---

#### _getAll_

In getAll method courses are not shown

**Request**

URL: `GET localhost:9001/students`

**Response**

Status: **200**

```json
[{"id":"19","name":"sda411134144f","phone":"0454545656565","email":"ha222ee@jbt.com","image":null},
{"id":"20","name":"sdqqqq4144f","phone":"0454545656565","email":"ha5555ee@jbt.com","image":null}]
```

---

#### _get_

In get method student's courses returned as array of course ids values

**Request**

URL: `GET localhost:9001/students/24`

**Response**

Status: **200**

```json
{"id":"33",
"name":"sd43534244f",
"phone":"0454545656565",
"email":"h43445e@jbt.com",
"image":null,
"courses":["22","23"]}
```

---

#### _create_

Students course id's should be provided as array of int id values.

**Request**

URL: `POST localhost:9001/students`

```javascript
// expected keys  ['name','phone','email','image','courses']
{
"name":"sdq232244f",
"phone":"0454545656565",
"email":"h5444g5e@jbt.com", // must bevalid email, required
"image":"http://google.com/image.png",
"courses":[22,24] // must be array
}
```

**Response**

Status: **201**

```json
{"status":"created","message":"Created","id":"15"}
```

---

#### _update_

If `courses` property provided as array in body, student's courses will be re-assigned (some will be added, some removed)

**Request**

URL: `PUT localhost:9001/students/15`

```javascript
// expected keys  ['name','phone','email','image','courses']
{
"name":"sdq232244f",
"phone":"0454545656565",
"email":"h5444g5e@jbt.com", // must bevalid email, required
"image":"http://google.com/image.png",
"courses":[22,24] // must be array
}
```

**Response**

Status: **200**

```json
{"status":"ok","message":"Ok"}
```

---

#### _delete_

**Request**

URL: `DELETE localhost:9001/students/2`

**Response**

Status: **204**

```json
{"status":"ok","message":"Ok"}
```
