<?php
return [
    'settings' => [
        'displayErrorDetails' => true,
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header


        'pdo' => [
            'dsn' => 'mysql:host=mysql;dbname=theschool;charset=utf8',
            'username' => 'root',
            'password' => 'helloomer',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],
    ],
];
