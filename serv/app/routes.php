<?php

use App\Controllers\AdminController;
use App\Controllers\CoursesController;
use App\Controllers\StudentsController;

$app->group('/admin', function ()  {
    $this->get   ('',             AdminController::class.':getAll');
    $this->get   ('/{id:[0-9]+}', AdminController::class.':get');
    $this->post  ('',             AdminController::class.':add')
        ->add(AdminController::class.':validate');
    $this->put   ('/{id:[0-9]+}', AdminController::class.':update')
        ->add(AdminController::class.':validate');
    $this->delete('/{id:[0-9]+}', AdminController::class.':delete');
})->add(AdminController::class.':scoping');

$app->group('/courses', function ()  {
    $this->get   ('',             CoursesController::class.':getAll');
    $this->get   ('/{id:[0-9]+}', CoursesController::class.':get');
    $this->post  ('',             CoursesController::class.':add')
        ->add(CoursesController::class.':validate');
    $this->put   ('/{id:[0-9]+}', CoursesController::class.':update')
        ->add(CoursesController::class.':validate');
    $this->delete('/{id:[0-9]+}', CoursesController::class.':delete');
})->add(CoursesController::class.':scoping');

$app->group('/students', function ()  {
    $this->get   ('',             StudentsController::class.':getAll');
    $this->get   ('/{id:[0-9]+}', StudentsController::class.':get');
    $this->post  ('',             StudentsController::class.':add')
        ->add(StudentsController::class.':validate');
    $this->put   ('/{id:[0-9]+}', StudentsController::class.':update')
        ->add(StudentsController::class.':validate');
    $this->delete('/{id:[0-9]+}', StudentsController::class.':delete');
})->add(StudentsController::class.':scoping');


