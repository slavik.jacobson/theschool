<?php
// DIC configuration

$container = $app->getContainer();
require_once dirname(__FILE__) . '/src/Controllers/BaseController.php';
require_once dirname(__FILE__) . '/src/Controllers/AdminController.php';
require_once dirname(__FILE__) . '/src/Controllers/CoursesController.php';
require_once dirname(__FILE__) . '/src/Controllers/StudentsController.php';
require_once dirname(__FILE__) . '/src/Controllers/RestController.php';
require_once dirname( __FILE__ ) . '/src/Model/Model.php';
require_once dirname( __FILE__ ) . '/src/Model/AdminModel.php';
require_once dirname( __FILE__ ) . '/src/Model/CoursesModel.php';
require_once dirname( __FILE__ ) . '/src/Model/StudentsModel.php';
use App\Controllers\AdminController;
use App\Controllers\CoursesController;
use App\Controllers\StudentsController;
use App\Model\Model;
use App\Model\AdminModel;
use App\Model\CoursesModel;
use App\Model\StudentsModel;

$container['pdo'] = function ($c) {
    $settings = $c->get('settings')['pdo'];
    return new PDO($settings['dsn'], $settings['username'], $settings['password']);
};

$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};


$container['App\Controllers\AdminController'] = function ($c) {
    return new AdminController($c->get('logger'), $c->get('App\Model\AdminModel'));
};

$container['App\Controllers\CoursesController'] = function ($c) {
    return new CoursesController($c->get('logger'), $c->get('App\Model\CoursesModel'));
};

$container['App\Controllers\StudentsController'] = function ($c) {
    return new StudentsController($c->get('logger'), $c->get('App\Model\StudentsModel'));
};


$container['App\Model\AdminModel'] = function ($c) {
    return new AdminModel($c->get('logger'), $c->get('pdo'), '');
};

$container['App\Model\CoursesModel'] = function ($c) {
    return new CoursesModel($c->get('logger'), $c->get('pdo'), '');
};

$container['App\Model\StudentsModel'] = function ($c) {
    return new StudentsModel($c->get('logger'), $c->get('pdo'), '');
};
