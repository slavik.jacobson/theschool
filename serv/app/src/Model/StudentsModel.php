<?php

namespace App\Model;
use Psr\Log\LoggerInterface;
use PDO;
use App\Model\Model as Model;

class StudentsModel extends Model
{

    public function __construct(LoggerInterface $logger, PDO $pdo, $table)
    {
        parent::__construct($logger,$pdo, $table);
    }

    public function getAll($path, $arrparams)
    {
        return parent::getAll($path, $arrparams);
    }

    private function _insertCourses($insert_id, $courses, $error) {
        $columns = array('student_id', 'course_id');
        $params = [];
        $insert_values = [];
        foreach ($courses as $i => $val){
            $params[] = '(:student_id'  . strval($i) . ', :course_id' .  strval($i) . ')';
            $insert_values = array_merge($insert_values, array(
                'student_id'. strval($i) => intval($insert_id),
                'course_id'. strval($i) => $val
            ));
        }
        $sql = "INSERT INTO students_to_courses (" . implode(',', $columns ) . ") VALUES " . implode(',', $params);

        $stmt = $this->pdo->prepare($sql);
        foreach($insert_values as $key => $value){
            $stmt->bindValue(':' . $key,  $value, PDO::PARAM_INT);
        }
        $stmt->execute();
//        print_r(array('ins' => $insert_values, 'sql' => $sql,'Ierr' => $stmt->errorInfo()));
        $insertError = !$error ? $stmt->errorInfo()[2] : $error;
        return $insertError;
        // print_r(array('err' => $stmt->errorInfo()));
    }

    private function _deleteCourses($insert_id, $courses) {

        $in_str = implode(',', $courses);

        $sql = "DELETE FROM students_to_courses WHERE student_id = :student_id AND course_id IN (:course_ids)";

        $stmt = $this->pdo->prepare($sql);

        $stmt->bindValue(':student_id', $insert_id, PDO::PARAM_INT );
        $stmt->bindValue(':course_ids', $in_str );

        $stmt->execute();
        echo '\n';print_r(array('sql' => $sql,'Derr' => $stmt->errorInfo()));
        return $stmt->errorInfo()[2];
    }

    private function _getCourses($id, $getResult = null) {

        $sql = "SELECT course_id FROM students_to_courses WHERE student_id = :student_id";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(':student_id', $id);
        $stmt->execute();

        if ($stmt) {

            $courses = array();
            while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
                $courses[] = $row['course_id'];
            }

            if ($getResult != null) {
                $getResult['courses'] = $courses;
                $result = $getResult;
            } else $result = $courses;

            return $result;
        } else {
            return null;
        }

    }

    public function get($path, $args)
    {

        $result = parent::get($path, $args);
        return $this->_getCourses($args['id'], $result);

    }


    public function update($path, $args, $request_data)
    {
        $setCourses = false;
        $newCourses = [];
        $oldCourses = $this->_getCourses($args['id']);

        if (array_key_exists('courses',$request_data)) {
            $newCourses =  $request_data['courses'];
            $setCourses = true;
            unset( $request_data['courses'] );
        }

        $result = parent::update($path, $args, $request_data);

        if ($result['updated'] && $setCourses) {
            $insertCourses = array_diff($newCourses, $oldCourses);
            $deleteCourses = array_diff($oldCourses, $newCourses);

            if (sizeof( $insertCourses )) {
                $insertError = $this->_insertCourses($args['id'], $insertCourses, false);
                print_r(array('insertError' => $insertError ));
                if ($insertError != '') $result['errors'][] = $insertError;
            }

            if (sizeof( $deleteCourses ))  {

                if (sizeof($result['errors']) < 1) {
                    $delError = $this->_deleteCourses($args['id'], $deleteCourses);
                    if ($delError != '') $result['errors'][] = $delError;
                }
            }
        }

        return $result;

    }

    public function delete($path, $args)
    {
        return parent::delete($path, $args);
    }
    /**
     * @param array $request_data
     *
     * @return int (last inserted id)
     */
    public function add($path, $request_data)
    {
        $courses = [];
        if (array_key_exists('courses',$request_data)) {
            $courses =  $request_data['courses'];
            unset( $request_data['courses'] );
        }


        $result = parent::add($path, $request_data);
        $error = $result['error'];
        if(($result['lastInsertId'] > 0) && (sizeof($courses) > 0))
            $error = $this->_insertCourses($result['lastInsertId'], $courses, $error);
        return array('lastInsertId' => $result['lastInsertId'],'error' => $error);

    }

}