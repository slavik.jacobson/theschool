<?php

namespace App\Model;

use Psr\Log\LoggerInterface;
use PDO;
use App\Model\Model as Model;

class AdminModel extends Model
{
    public function __construct(LoggerInterface $logger, PDO $pdo, $table)
    {
        return parent::__construct($logger,$pdo, $table);
    }

    public function getAll($path, $arrparams)
    {
        return parent::getAll($path, $arrparams);
    }

    /**
     * @param int $id
     *
     * @return one object
     */
    public function get($path, $args)
    {
        return parent::get($path, $args);
    }
    /**
     * @param array $request_data
     *
     * @return int (last inserted id)
     */
    public function add($path, $request_data)
    {
        $request_data['password'] = password_hash($request_data['password'] , PASSWORD_DEFAULT);
        return parent::add($path, $request_data);
    }
    /**
     * @param array $request_data
     *
     * @return bool
     */
    public function update($path, $args, $request_data)
    {
        if(array_key_exists('password', $request_data))
            $request_data['password'] = password_hash($request_data['password'] , PASSWORD_DEFAULT);
        return parent::update($path, $args, $request_data);
    }
    /**
     * @param int pk
     *
     * @return bool
     */
    public function delete($path, $args)
    {
        return parent::delete($path, $args);
    }
}