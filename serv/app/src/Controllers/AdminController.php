<?php

namespace App\Controllers;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use App\Controllers\BaseController;
use App\Controllers\RestController as Rest;
use App\Model\AdminModel as Model;

class AdminController extends BaseController
{

    /**
     * @var user
     */
    protected $user;
    /**
     * @param \Psr\Log\LoggerInterface       $logger
     * @param \App\Model                $model
     * @param                                $server
     */
    public function __construct(LoggerInterface $logger, Model $model)
    {


        parent::__construct($logger,$model);
    }

    /**
     * @param Request $req
     * @param Response $res
     * @param $next
     * @return mixed
     */
    public function authenticate(Request $req, Response $res, $next){

        $server_params = $req->getServerParams();
        preg_match("/Basic\s+(.*)$/i", $server_params['REDIRECT_HTTP_AUTHORIZATION'], $matches);
        try{

            list($user, $password) = explode(":", base64_decode($matches[1]), 2);
        } catch (\Exception $e){

            return Rest::notAuthenticated($req, $res);
        }

        $admins = $this->model->getAll('admin', array());

        if ($admins == null ) {
            return Rest::notAuthenticated($req, $res);
        }
        $result = false;

        foreach ($admins as $key => $value) {

            if ($value['name'] == $user && password_verify($password, $value['password']) ) {

                $req = $req->withAttribute('admin_user', $value);
                $result = true;
            }
        }
        if ($result == false) {
            return Rest::notAuthenticated($req, $res);
        } else return $next($req, $res);
    }


    public function validate(Request $request, Response $response, $next)
    {
        $request_data = $request->getParsedBody();
        $roleEnum = array("manager", "sales");

        $expected = ['name','phone','role','image','email','password'];
        $unexpected = array_diff(array_keys($request_data), $expected);
        if(count($unexpected) > 0) return Rest::invalid($request, $response, 'Unexpected keys: '.implode(',',$unexpected));

        if (!array_key_exists('email', $request_data) || !array_key_exists('role', $request_data) || !array_key_exists('name', $request_data) )  return Rest::invalid($request, $response);
        if (!filter_var($request_data['email'], FILTER_VALIDATE_EMAIL)) return Rest::invalid($request, $response);
        if (!in_array($request_data['role'], $roleEnum))  return Rest::invalid($request, $response);

        return $next($request,$response);

    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $next
     * @return mixed
     */
    public function scoping(Request $request, Response $response, $next)
    {

        $admin_user = $request->getAttribute('admin_user');
        $route = $request->getAttribute('route');
        switch($request->getMethod()){
            case 'GET':
                $id = $route->getArgument('id');
                if ( $id == 1 && ($admin_user['role'] == 'owner') ) return $next($request,$response);
                if ( $id != 1 && ($admin_user['role'] != 'sales') ){return $next($request,$response);}
                else return Rest::forbidden($request, $response);
                break;
            case 'PUT':
                $id = $route->getArgument('id');
                if (($admin_user['role'] == 'sales') ||
                    ($id == 1) ||
                    ($id == $admin_user['id']) && ($admin_user['role'] != 'owner'))
                {return Rest::forbidden($request, $response);}
                else return $next($request,$response);
                break;
            case 'POST':
                $request_data = $request->getParsedBody();
                if ($request_data['role'] == 'owner')  return Rest::forbidden($request, $response);
                if (($admin_user['role'] != 'sales') ) {return $next($request,$response);}
                else return Rest::forbidden($request, $response);
//                code to be executed if n=label3;
                break;
            case 'DELETE':
                $id = $route->getArgument('id');
                if (($id == $admin_user['id']) && ($admin_user['role'] != 'owner')) return Rest::forbidden($request, $response);
                if (($admin_user['role'] != 'sales') ) {return $next($request,$response);}
                else return Rest::forbidden($request, $response);
                break;
            default:
                return Rest::forbidden($request, $response);
//                code to be executed if n is different from all labels;
        }

    }

    public function getAll(Request $request, Response $response, $args, $del = ['password']) {

        return  parent::getAll($request, $response, $args, $del);

    }


    public function get(Request $request, Response $response, $args)
    {

        return parent::get($request, $response, $args);

    }

    public function add(Request $request, Response $response, $args)
    {

        return parent::add($request, $response, $args);


    }

    public function update(Request $request, Response $response, $args)
    {

        return parent::update($request, $response, $args);

    }

    public function delete(Request $request, Response $response, $args)
    {

        return  parent::delete($request, $response, $args);

    }

}