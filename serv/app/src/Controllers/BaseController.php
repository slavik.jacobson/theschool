<?php

namespace App\Controllers;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use App\Controllers\RestController as Rest;
use App\Model\Model;
/**
 * Class _Controller.
 */
abstract class BaseController
{

    abstract public function validate(Request $request, Response $response, $next);
//    abstract public function read(Request $request, Response $response, $next);
//    abstract public function update(Request $request, Response $response, $next);

//    public function authorize(Request $request, Response $response, $next) {
//        $response =  $next($request, $response);
//        return $response;
//    }
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;
    /**
     * @var \App\Model
     */
    protected $model;
    /**
     * @param \Psr\Log\LoggerInterface       $logger
     * @param \App\Model                $model
     */
    public function __construct(LoggerInterface $logger, Model $model)
    {
        $this->logger = $logger;
        $this->model = $model;
    }


    /**
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param \Psr\Http\Message\ResponseInterface      $response
     * @param array                                    $args
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getAll(Request $request, Response $response, $args, $del = [])
    {
        $this->logger->info(substr(strrchr(rtrim(__CLASS__, '\\'), '\\'), 1).': '.__FUNCTION__);
        $path = explode('/', $request->getUri()->getPath())[1];
        $arrparams = $request->getParams();
        $data = $this->model->getAll($path, $arrparams);
        $filteredData = sizeof($del) > 0 ? [] : $data;
        foreach($data as $row){
            foreach ($del as $value) {
                unset($row[$value]);
                $filteredData[] = $row;
            }
        }

        return $response->write(json_encode($filteredData));
    }
    /**
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param \Psr\Http\Message\ResponseInterface      $response
     * @param array                                    $args
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function get(Request $request, Response $response, $args)
    {
        $this->logger->info(substr(strrchr(rtrim(__CLASS__, '\\'), '\\'), 1).': '.__FUNCTION__);
        $path = explode('/', $request->getUri()->getPath())[1];
        $result = $this->model->get($path, $args);
        if ($result == null) {
            return $response ->withStatus(404);
        } else {
            return $response->write(json_encode($result));
        }
    }
    /**
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param \Psr\Http\Message\ResponseInterface      $response
     * @param array                                    $args
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function add(Request $request, Response $response, $args)
    {
        $this->logger->info(substr(strrchr(rtrim(__CLASS__, '\\'), '\\'), 1).': '.__FUNCTION__);
        $path = explode('/', $request->getUri()->getPath())[1];
        $request_data = $request->getParsedBody();
//        print_r($request_data);
        $result = $this->model->add($path, $request_data);
        if ($result['lastInsertId'] > 0) {
            $RequesPort = '';
            if ($request->getUri()->getPort()!='')
            {
                $RequesPort = '.'.$request->getUri()->getPort();
            }
            $LocationHeader = $request->getUri()->getScheme().'://'.$request->getUri()->getHost().$RequesPort.$request->getUri()->getPath().'/'.$result['lastInsertId'];
            return Rest::created($request, $response, $LocationHeader, $result['lastInsertId'], $result['error']);
        } else {
            return Rest::badRequest($request, $response, $result['error']);
        }
    }
    /**
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param \Psr\Http\Message\ResponseInterface      $response
     * @param array                                    $args
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function update(Request $request, Response $response, $args)
    {
        $this->logger->info(substr(strrchr(rtrim(__CLASS__, '\\'), '\\'), 1).': '.__FUNCTION__);
        $path = explode('/', $request->getUri()->getPath())[1];
        $request_data = $request->getParsedBody();
        $result = $this->model->update($path, $args, $request_data);
        if ($result['updated']) {
            return Rest::ok($request, $response, $result['errors']);
        } else {
            return Rest::badRequest($request, $response, $result['errors'] );
        }
    }
    /**
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param \Psr\Http\Message\ResponseInterface      $response
     * @param array                                    $args
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function delete(Request $request, Response $response, $args)
    {
        $this->logger->info(substr(strrchr(rtrim(__CLASS__, '\\'), '\\'), 1).': '.__FUNCTION__);
        $path = explode('/', $request->getUri()->getPath())[1];
        $isdeleted = $this->model->delete($path, $args);
        if ($isdeleted) {
            return $response ->withStatus(204);
        } else {
            return Rest::notFound($request, $response);
        }
    }
}