<?php
namespace App\Controllers;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class RestController {
    /**
     * @var null
     */
    protected static $_instance = null;

    protected function __construct() {}

    /**
     * @param Request $req
     * @param Response $res
     * @return mixed
     */
    public static function notAuthenticated(Request $req, Response $res) {
        $data = [];
        $data["status"] = "error";
        $data["message"] = 'Authentication Failed';
        return $res->withStatus(401)->write(json_encode($data, JSON_UNESCAPED_SLASHES));
    }

    /**
     * @param Request $req
     * @param Response $res
     * @return mixed
     */
    public static function forbidden(Request $req, Response $res) {
        $data = [];
        $data["status"] = "error";
        $data["message"] = 'Action Forbidden';
        return $res->withStatus(403)->write(json_encode($data, JSON_UNESCAPED_SLASHES));
    }

    /**
     * @param Request $req
     * @param Response $res
     * @param $details
     * @return mixed
     */
    public static function invalid(Request $req, Response $res, $details = null) {
        $data = [];
        $data["status"] = "error";
        $data["message"] = 'Invalid Data';
        $data["details"] = $details;
        return $res->withStatus(422)->write(json_encode($data, JSON_UNESCAPED_SLASHES));
    }


    /**
     * @param Request $req
     * @param Response $res
     * @return mixed
     */
    public static function notFound(Request $req, Response $res) {
        $data = [];
        $data["status"] = "error";
        $data["message"] = 'Not Found';
        return $res->withStatus(404)->write(json_encode($data, JSON_UNESCAPED_SLASHES));
    }

    /**
     * @param Request $req
     * @param Response $res
     * @return mixed
     * @param $details
     */
    public static function badRequest(Request $req, Response $res, $details = null) {
        $data = [];
        $data["status"] = "error";
        $data["message"] = 'Bad Request';
        $data["details"] = $details;
        return $res->withStatus(400)->write(json_encode($data, JSON_UNESCAPED_SLASHES));
    }

    /**
     * @param Request $req
     * @param Response $res
     * @param $LocationHeader
     * @param $id
     * @param $details
     * @return mixed
     */
    public static function created(Request $req, Response $res, $LocationHeader, $id = null, $details = null) {
        $data = [];
        $data["status"] = $details ? "created with errors" : "created";
        $data["message"] = 'Created';
        if ($id != null) $data["id"] = $id;
        if ($details) $data["details"] = "Wrong reference id value provided";
        return $res->withStatus(201) ->withHeader('Location', $LocationHeader)->write(json_encode($data, JSON_UNESCAPED_SLASHES));
    }

    /**
     * @param Request $req
     * @param Response $res
     * @param $details
     * @return mixed
     */
    public static function ok(Request $req, Response $res, $details = null) {
        $data = [];
        $data["status"] = sizeof($details) > 0 ? "ok with errors" : "ok";
        $data["message"] = 'Ok';
        if ($details) $data["details"] = $details;
        return $res->withStatus(200)->write(json_encode($data, JSON_UNESCAPED_SLASHES));
    }
}