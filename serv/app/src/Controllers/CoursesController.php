<?php

namespace App\Controllers;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use App\Controllers\BaseController;
use App\Controllers\RestController as Rest;
use App\Model\CoursesModel as Model;

class CoursesController extends BaseController
{

    /**
     * @param \Psr\Log\LoggerInterface       $logger
     * @param \App\Model                $model
     * @param                                $server
     */
    public function __construct(LoggerInterface $logger, Model $model)
    {
        parent::__construct($logger,$model);
    }


    public function validate(Request $request, Response $response, $next)
    {
        $request_data = $request->getParsedBody();

        $expected = ['name','description','image'];
        $unexpected = array_diff(array_keys($request_data), $expected);
        if(count($unexpected) > 0) return Rest::invalid($request, $response, 'Unexpected keys: '.implode(',',$unexpected));

        if ( !array_key_exists('name', $request_data) ) return Rest::invalid($request, $response);
        return $next($request,$response);

    }




    /**
     * @param Request $request
     * @param Response $response
     * @param $next
     * @return mixed
     */
    public function scoping(Request $request, Response $response, $next)
    {
        $admin_user = $request->getAttribute('admin_user');

        switch($request->getMethod()){
            case 'GET':
                return $next($request,$response);
                break;
            case 'PUT':
                if ($admin_user['role'] == 'sales'){
                    return Rest::forbidden($request, $response);
                }
                else return $next($request,$response);
                break;
            case 'POST':
                if ($admin_user['role'] == 'sales') {
                    return Rest::forbidden($request, $response);
                }
                else return $next($request,$response);
                break;
            case 'DELETE':
                if ($admin_user['role'] == 'sales'){
                    return Rest::forbidden($request, $response);
                }
                else return $next($request,$response);
                break;
            default:
                return Rest::forbidden($request, $response);
//                code to be executed if n is different from all labels;
        }

    }



    public function getAll(Request $request, Response $response, $args, $del = []) {

        return  parent::getAll($request, $response, $args, $del);

    }


    public function get(Request $request, Response $response, $args)
    {

        return parent::get($request, $response, $args);

    }

    public function add(Request $request, Response $response, $args)
    {

        return parent::add($request, $response, $args);


    }

    public function update(Request $request, Response $response, $args)
    {

        return  parent::update($request, $response, $args);

    }

    public function delete(Request $request, Response $response, $args)
    {

        return parent::delete($request, $response, $args);

    }
}